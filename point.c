#include <stdio.h>
#include <math.h>

typedef struct {
    double x;
    double y;
} Point;

void pointPrint(Point a) {
    printf("(%g, %g)", a.x, a.y);
}

int pointEqual(Point a, Point b) {
    if ( a.x == b.x || a.y == b.y ) {
        return 1;
    }
    
    return 0;
}

double pointDistance(Point a, Point b) {
    return hypot(a.x-b.x, a.y-b.y);
}

int main() {
    Point a = { 3, 5 };
    Point b = { 4, 8 };
    
    pointPrint(a);
    printf("%d\n", pointEqual(a, b));
    printf("%g\n", pointDistance(a, b));
    
    return 0;
    
}